{
  "$schema": "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json",
  "transient_prompt": {
    "template": " \u251c\u2500\u22a1 ",
    "foreground": "blue",
    "background": "transparent"
  },
  "blocks": [
    {
      "type": "prompt",
      "alignment": "left",
      "segments": [
        {
          "properties": {
            "cache_duration": "none"
          },
          "leading_diamond": "\u256d\u2500\ue0b6",
          "trailing_diamond": "\ue0b0",
          "template": " \ue691 ",
          "foreground": "lightWhite",
          "background": "blue",
          "type": "text",
          "style": "diamond"
        },
        {
          "properties": {
            "cache_duration": "none",
            "folder_icon": "\udb80\ude4b ",
            "folder_separator_icon": "<transparent> \ue0bd  </>",
            "style": "full"
          },
          "leading_diamond": "<transparent>\ue0b0</>",
          "trailing_diamond": "\ue0b0",
          "template": " {{ .Path }} ",
          "foreground": "lightWhite",
          "background": "magenta",
          "type": "path",
          "style": "diamond"
        },
        {
          "properties": {
            "branch_ahead_icon": "\udb82\udd07 ",
            "branch_behind_icon": "\udb83\udd3b ",
            "branch_icon": "\ue725 ",
            "branch_identical_icon": "\udb82\ude50 ",
            "branch_max_length": 25,
            "cache_duration": "none",
            "fetch_stash_count": true,
            "fetch_status": true,
            "fetch_upstream_icon": true,
            "git_icon": "\uf1d3 "
          },
          "template": " {{ .UpstreamIcon }} {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }} {{ end }}<,darkGray>{{ if .Working.Changed }} {{ if gt .Working.Unmerged 0 }}<magenta>\uf467 {{ .Working.Unmerged }}</> {{ end }}{{ if gt .Working.Deleted 0 }}<lightRed>\uf1f8 {{ .Working.Deleted }}</> {{ end }}{{ if gt .Working.Added 0 }}<green>\ue676 {{ .Working.Added }}</> {{ end }}{{ if gt .Working.Modified 0 }}<yellow>\udb80\udfeb {{ .Working.Modified }}</> {{ end }}{{ if gt .Working.Untracked 0 }}<lightCyan>\udb82\udf59 {{ .Working.Untracked }}</> {{ end }}{{ end }}</>{{ if and (.Working.Changed) (.Staging.Changed) }}{{ end }}{{ if .Staging.Changed }}<,green> <darkGray>\uee19 {{ add .Staging.Unmerged .Staging.Deleted .Staging.Added .Staging.Modified .Staging.Untracked }}</> </>{{ end }}{{ if gt .StashCount 0 }}<,red> <lightWhite>\ued75 {{ .StashCount }} </></>{{ end }} ",
          "foreground": "darkGray",
          "powerline_symbol": "\ue0b0",
          "background": "lightGreen",
          "type": "git",
          "style": "powerline",
          "foreground_templates": [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}darkGray{{ end }}",
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}lightWhite{{ end }}",
            "{{ if gt .Ahead 0 }}darkGray{{ end }}",
            "{{ if gt .Behind 0 }}darkGray{{ end }}"
          ],
          "background_templates": [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}yellow{{ end }}",
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}red{{ end }}",
            "{{ if gt .Ahead 0 }}lightMagenta{{ end }}",
            "{{ if gt .Behind 0 }}lightMagenta{{ end }}"
          ]
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": "·",
          "foreground": "black",
          "type": "text",
          "style": "plain"
        }
      ],
      "newline": true
    },
    {
      "type": "prompt",
      "alignment": "right",
      "filler": "<darkGray,transparent>\u22c5</>",
      "overflow": "hide",
      "segments": [
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": " <lightCyan>\udb81\udfd3</> {{ .Major }} ",
          "type": "go",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": " <lightBlue>\ue738</> {{ .Major }} ",
          "type": "java",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none",
            "fetch_version": true
          },
          "template": " <lightGreen>\udb80\udf99</> {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Major }} ",
          "type": "node",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": " <lightRed>\ue71e</> {{ .Major }} ",
          "type": "npm",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": "  <lightMagenta>\ue67e</> {{ .Major }} ",
          "type": "perl",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": " <lightMagenta>\ued6d</> {{ .Major }} ",
          "type": "php",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": " <lightYellow>\ue73c</> {{ .Major }} ",
          "type": "python",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": " <cyan>\udb81\udf08</> {{ .Major }} ",
          "type": "react",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": " <red>\ue739</> {{ .Major }} ",
          "type": "ruby",
          "style": "powerline"
        },
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": " <lightBlue>\ue6a7</> {{ .Major }} ",
          "type": "yarn",
          "style": "powerline"
        },
        {
          "properties": {
            "always_enabled": true,
            "cache_duration": "none",
            "style": "austin",
            "threshold": 500
          },
          "leading_diamond": "\ue0b2",
          "trailing_diamond": "\ue0d6",
          "template": " \udb86\udd25 {{ .FormattedMs }} ",
          "foreground": "darkGray",
          "background": "white",
          "type": "executiontime",
          "style": "diamond"
        },
        {
          "properties": {
            "always_enabled": true,
            "cache_duration": "none"
          },
          "leading_diamond": "\ue0b2",
          "trailing_diamond": "\ue0b4",
          "template": " {{ if eq .Code 0 }}\uf00c{{ else }}\uf00d{{ end }} ",
          "foreground": "lightWhite",
          "background": "green",
          "type": "status",
          "style": "diamond",
          "background_templates": [
            "{{ if .Error }}red{{ end }}"
          ]
        }
      ]
    },
    {
      "type": "prompt",
      "alignment": "left",
      "segments": [
        {
          "properties": {
            "cache_duration": "none"
          },
          "template": "\u2570\u2500",
          "foreground": "blue",
          "type": "text",
          "style": "plain"
        }
      ],
      "newline": true
    }
  ],
  "version": 3,
  "final_space": true
}
