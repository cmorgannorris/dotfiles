# Volta configuration
export VOLTA_HOME=$HOME/.volta

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=$VOLTA_HOME/bin:/opt/homebrew/bin:/opt/homebrew/sbin:$PATH

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/homebrew/Caskroom/miniconda/base/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/homebrew/Caskroom/miniconda/base/etc/profile.d/conda.sh" ]; then
        . "/opt/homebrew/Caskroom/miniconda/base/etc/profile.d/conda.sh"
    else
        export PATH="/opt/homebrew/Caskroom/miniconda/base/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# Add trellis-cli to your shell.
if command -v trellis 1>/dev/null 2>&1; then
    emulate zsh -c "$(trellis shell-init zsh)"
fi

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh


# FZF default command
export FZF_DEFAULT_COMMAND="rg --files --ignore-vcs"
export FZF_DEFAULT_OPTS=" --layout=reverse --info=right --height=80% --padding=1 --multi --preview '([[ -f {} ]] && (bat --style=grid,numbers,header --color=always --line-range=:500 {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200' --color=fg:#edeeec,bg:#343434,hl:#c2d5fb --color=fg+:#feffff,bg+:#535353,hl+:#e0f3fd --color=info:#aac16d,prompt:#c25a56,pointer:#e87a94 --color=marker:#c7df82,spinner:#c899d4,header:#f6c87a --prompt=' ' --pointer='' --marker='' --bind '?:toggle-preview' --bind 'ctrl-a:select-all' --bind 'ctrl-y:execute-silent(echo {+} | pbcopy)' --bind 'ctrl-e:execute(echo {+} | xargs -o vim)' --bind 'ctrl-v:execute(code {+})'"

export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# Set default editors
export EDITOR=/opt/homebrew/bin/vim
export VISUAL=/opt/homebrew/bin/vim

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME=""

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    brew
    git
    git-flow
    jump
    macos
    rsync
    tmux
    vagrant
    zsh-autosuggestions
    zsh-syntax-highlighting
)

# Specify completion file location so it doesn't clutter home folder
export ZSH_COMPDUMP=~/.cache/zcompdump-$ZSH_VERSION

# Sources
source $ZSH/oh-my-zsh.sh

# zsh completions
fpath=(/opt/homebrew/share/zsh-completions $fpath)

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
alias dola="doctl compute droplet list --format 'ID,Name,PublicIPv4,Image,Status'"
alias dols="doctl compute droplet list"
alias dorb="doctl compute droplet-action reboot"
alias dossh="doctl compute ssh"
alias el="eza -hla --git --group --group-directories-first --no-user --octal-permissions --icons"
alias ez="vim ~/.zshrc"
alias gs="git -c delta.side-by-side=true"
alias jp="jump"
alias lg="lazygit"
alias ohmyzsh="vim ~/.oh-my-zsh"
alias rsdel="rsync -havzPui --delete --exclude='.DS_Store'"
alias rsdo="rsync -havzPui --exclude='.DS_Store'"
alias rsdry="rsync -havzPuin --delete --exclude='.DS_Store'"
alias ytmusic="yt-dlp --ignore-errors --format bestaudio --extract-audio --audio-format mp3 --audio-quality 160K --output '%(title)s.%(ext)s'"
alias ytplaylist="yt-dlp --ignore-errors --format bestaudio --extract-audio --audio-format mp3 --audio-quality 160K --output '%(title)s.%(ext)s' --yes-playlist"

# Fuzzy finder
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# yazi wrapper
function y() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		builtin cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

source $HOME/.config/broot/launcher/bash/br

# Add pyenv init to your shell to enable shims and autocompletion.
# Please make sure eval "$(pyenv init -)" is placed toward the end of the
# shell configuration file since it manipulates PATH during the initialization.
if command -v pyenv 1>/dev/null 2>&1; then
    # export PYENV_ROOT="$HOME/.pyenv"
    # export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init --path)"
    eval "$(pyenv init -)"
fi

# oh my posh
if [ "$TERM_PROGRAM" != "Apple_Terminal" ]; then
  eval "$(oh-my-posh init zsh --config ~/.crow.omp.json)"
fi

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# zoxide
eval "$(zoxide init zsh)"
source ~/.yazi.sh
