# dotfiles

![Screenshot of my shell prompt](https://i.imgur.com/PclmJd5.png)

## TLDR Quick Guide

#### .dotfile Symlink Script
```bash
git clone https://gitlab.com/cmorgannorris/dotfiles.git .dotfiles && cd .dotfiles && ./setup
```

#### Homebrew Install Script
```bash
./.brew
```

#### macOS Settings Script
```bash
./.macos
```

## macOS Applications
- ATTO Disk Benchmark
- Ableton Live 11 Suite
- Adobe
    - Acrobat DC
    - After Effects 2021
    - After Effects 2022
    - Audition 2022
    - Creative Cloud
    - Illustrator 2022
    - InDesign 2022
    - Lightroom Classic
    - Media Encoder 2022
    - Photoshop 2022
    - Premiere Pro 2022
- Aegisub
- AppCleaner
- Audacity
- Audio Hijack
- Beamer
- Burp Suite Community Edition
- Cisco
- Daggerfall
- DaisyDisk
- Discord
- Dragonframe 5
- Elgato Video Capture
- Fiery Driver Updater
- Firefox
- FontExplorer X Pro
- Game Capture HD
- GarageBand
- Google
    - Chrome
    - Docs
    - Drive
    - Sheets
    - Slides
- HandBrake
- ImageOptim
- MacFamilyTree 9
- MakeMKV
- Maxon Cinema 4D R25
- Microsoft
    - Excel
    - PowerPoint
    - Teams
    - Word
- MuseScore 3
- MusicBrainz Picard
- VPN
- OBS Link
- OBS
- Obsidian
- OpenEmu
- Operator
- Parallels
    - Desktop
    - Toolbox
- Photon_Workshop_V2.1.29
- Postman
- Safari
- Sequel Pro
- Slack
- Steam Link
- Steam
- Storyboarder
- TeamViewer
- The Unarchiver
- Transmission
- Transmit
- Typeface
- Utilities
- VLC
- VirtualBox
- Wacom Tablet.localized
- ZXP Installer
- calibre
- iMovie
- iTerm
- iZotope RX 8 Audio Editor
- logioptionsplus
- lotroclient
- zoom.us

## Setup

### Using Git and the setup script

Clone the repository wherever you like. The setup script will pull in the latest version and symlink the files to the home folder:

```bash
git clone https://gitlab.com/cmorgannorris/dotfiles.git .dotfiles && cd .dotfiles && ./setup
```

To update, `cd` into the local `.dotfiles` repository and then:

```bash
./setup
```

### Install Homebrew formulae

When setting up a new Mac, use this script to install [Homebrew](https://brew.sh/) and some formulae:

```bash
./.brew
```

### Sensible macOS defaults

When setting up a new Mac, use this script to set some sensible macOS defaults:

```bash
./.macos
```

### .gitconfig_local Setup

The .gitconfig file that's commited to this repo does not include user information. That info should be included in a .gitconfig_local file.

## Acknowledgments

* [Matthew Golden](https://github.com/ttamnedlog)
* [Mathias Bynens](https://github.com/mathiasbynens)
* [Ben Awad](https://github.com/benawad)

