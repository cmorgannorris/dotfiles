tap "brettferdosi/tap"
tap "eth-p/software"
tap "gromgit/fuse"
tap "homebrew/bundle"
tap "homebrew/services"
tap "jandedobbeleer/oh-my-posh"
tap "koekeishiya/formulae"
tap "mongodb/brew"
tap "roots/tap"
tap "tdsmith/ham"
tap "wpscanteam/tap"
# Color management engine supporting ICC profiles
brew "little-cms2"
# Tool for generating GNU Standards-compliant Makefiles
brew "automake"
# Clone of cat(1) with syntax highlighting and Git integration
brew "bat"
# High performance key/value database
brew "berkeley-db", link: true
# New way to see and navigate directory trees
brew "broot"
# GNU internationalization (i18n) and localization (l10n) library
brew "gettext"
# Cross-platform make
brew "cmake"
# Console Matrix
brew "cmatrix"
# Get a file from an HTTP, HTTPS or FTP server
brew "curl"
# Libraries to talk to Microsoft SQL Server and Sybase databases
brew "freetds"
# Graphics library to dynamically manipulate images
brew "gd"
# Network authentication protocol
brew "krb5"
# Postgres C API library
brew "libpq"
# General-purpose scripting language
brew "php"
# Dependency Manager for PHP
brew "composer"
# GNU File, Shell, and Text utilities
brew "coreutils"
# Reimplementation of ctags(1)
brew "ctags"
# Command-line tool for DigitalOcean
brew "doctl"
# Perl lib for reading and writing EXIF metadata
brew "exiftool"
# Database of public exploits and corresponding vulnerable software
brew "exploitdb"
# Modern, maintained replacement for ls
brew "eza"
# GNU Transport Layer Security (TLS) Library
brew "gnutls"
# Image processing and image analysis library
brew "leptonica"
# Play, record, convert, and stream audio and video
brew "ffmpeg"
# Interpreter for PostScript and PDF
brew "ghostscript"
# Distributed revision control system
brew "git"
# AVH edition of git-flow
brew "git-flow-avh"
# Git extension for versioning large files
brew "git-lfs"
# Open-source GitLab command-line tool
brew "glab"
# Generate introspection data for GObject libraries
brew "gobject-introspection"
# Directory/file & DNS busting tool written in Go
brew "gobuster"
# General-purpose lossless data-compression library
brew "zlib"
# Open source relational database management system
brew "mysql-client"
# Network logon cracker which supports many services
brew "hydra"
# ISO/IEC 23008-12:2017 HEIF file format decoder and encoder
brew "libheif"
# Library for reading RAW files from digital photo cameras
brew "libraw"
# Tools and libraries to manipulate images in many formats
brew "imagemagick"
# Modular IRC client
brew "irssi"
# Sophisticated file transfer program
brew "lftp"
# Portable Foreign Function Interface library
brew "libffi"
# X.509 and CMS library
brew "libksba"
# MNG/JNG reference library
brew "libmng"
# Linux virtual machines
brew "lima"
# Unified display of technical and tag data for audio/video
brew "media-info"
# Cross-platform application and UI framework
brew "qt"
# Matroska media files manipulation tools
brew "mkvtoolnix"
# Platform built on V8 to build network applications
brew "node"
# Fast, highly customisable system info script
brew "neofetch"
# Web server scanner
brew "nikto"
# Port scanning utility for large networks
brew "nmap"
# Object-relational database system
brew "postgresql@14", restart_service: true
# Code formatter for JavaScript, CSS, JSON, GraphQL, Markdown, YAML
brew "prettier"
# Protocol buffers (Google's data interchange format)
brew "protobuf"
# Monitor data's progress through a pipe
brew "pv"
# Python version management
brew "pyenv"
# Pyenv plugin to manage virtualenv
brew "pyenv-virtualenv"
# Interpreted, interactive, object-oriented programming language
brew "python@3.10"
# Perl-powered file rename script with many helpful built-ins
brew "rename"
# Search tool like grep and The Silver Searcher
brew "ripgrep"
# Utility that provides fast incremental file transfer
brew "rsync"
# Safe, concurrent, practical language
brew "rust"
# User interface to the TELNET protocol
brew "telnet"
# Powerful free data recovery utility
brew "testdisk"
# Terminal multiplexer
brew "tmux"
# Display directories as trees (with optional color/HTML output)
brew "tree"
# Vi 'workalike' with many additional features
brew "vim"
# JavaScript toolchain manager for reproducible environments
brew "volta"
# Internet file retriever
brew "wget"
# Command-line interface for WordPress
brew "wp-cli"
# Feature-rich command-line audio/video downloader
brew "yt-dlp"
# UNIX shell (command interpreter)
brew "zsh"
# Additional completion definitions for zsh
brew "zsh-completions"
# Fish shell like syntax highlighting for zsh
brew "zsh-syntax-highlighting"
# Bash scripts that integrate bat with various command-line tools
brew "eth-p/software/bat-extras"
# FUSE file system for mounting to another location
brew "gromgit/fuse/bindfs-mac"
# File system client based on SSH File Transfer Protocol
brew "gromgit/fuse/sshfs-mac"
# Prompt theme engine for any shell
brew "jandedobbeleer/oh-my-posh/oh-my-posh", link: false
# Simple hotkey-daemon for macOS.
brew "koekeishiya/formulae/skhd"
# A tiling window manager for macOS based on binary space partitioning.
brew "koekeishiya/formulae/yabai"
# High-performance, schema-free, document-oriented database
brew "mongodb/brew/mongodb-community"
# A CLI to manage Trellis projects
brew "roots/tap/trellis-cli"
# Black box WordPress vulnerability scanner
brew "wpscanteam/tap/wpscan"
# Distribution of the Python and R programming languages for scientific computing
cask "anaconda"
# Virtual Audio Driver
cask "blackhole-16ch"
# App to build and share containerised applications and microservices
cask "docker"
# Open Source Webfont Converter
cask "fontplop"
# File system integration
cask "macfuse"
# Minimal installer for conda
cask "miniconda"
# Knowledge base that works on top of a local folder of plain text Markdown files
cask "obsidian"
# rclone cask to work around lack of mount support in the formula version
cask "rclone-cask"
# Development environment
cask "vagrant"
