require("folder-rules"):setup()
require("relative-motions"):setup({ show_numbers="relative" })
require("git"):setup()
require("omp"):setup()
require("dual-pane"):setup()
require("mactag"):setup {
	-- Keys used to add or remove tags
	keys = {
		r = "Red",
		o = "Orange",
		y = "Yellow",
		g = "Green",
		b = "Blue",
		p = "Purple",
	},
	-- Colors used to display tags
	colors = {
		Red    = "#ee7b70",
		Orange = "#f5bd5c",
		Yellow = "#fbe764",
		Green  = "#91fc87",
		Blue   = "#5fa3f8",
		Purple = "#cb88f8",
	},
}
local home = os.getenv("HOME")
require("bunny"):setup({
  hops = {
    { tag = "home", path = home, key = "h" },
    { tag = "config", path = home.."/.config", key = "C" },
    { tag = "downloads", path = home.."~/Downloads", key = "d" },
  },
  notify = true, -- notify after hopping, default is false
})
require("custom-shell"):setup({
    history_path = "default",
    save_history = true,
})

Header:children_add(function()
	if ya.target_family() ~= "unix" then
		return ""
	end
	return ui.Span(ya.user_name() .. "@" .. ya.host_name() .. ":"):fg("blue")
end, 500, Header.LEFT)
